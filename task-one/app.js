function moveZeros(arr) {
  let nonZeroIndex = 0;
  let i = 0;

  while (i < arr.length) {
    if (arr[i] !== 0 && i !== nonZeroIndex) {
      [arr[i], arr[nonZeroIndex]] = [arr[nonZeroIndex], arr[i]];
    }

    nonZeroIndex += arr[nonZeroIndex] !== 0;
    i++;
  }

  return arr;
}

console.log(moveZeros([false, 1, 0, 1, 2, 0, 1, 3, "a"]));
