import SelectWithOption from './components/SelectWithOption'
import GlobalStyles from './theme/GlobalStyles'

const App = () => {
  return (
    <>
      <GlobalStyles />
      <SelectWithOption />
    </>
  )
}

export default App
