export const optionsConfig = [
  {
    id: 1,
    name: 'account',
  },
  {
    id: 2,
    name: 'wallet',
  },
  {
    id: 3,
    name: 'bonuses',
  },
  {
    id: 4,
    name: 'bets',
  },
  {
    id: 5,
    name: 'history',
  },
]
