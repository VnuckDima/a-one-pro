import styled from 'styled-components'

export const SelectWithOptionWrapper = styled.div`
  width: 240px;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
`
export const NormalizedWrapper = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`
