import { useRef, useState } from 'react'

import { useClickOutside } from '../../hooks/useClickOutside'
import ModalOptionsWrapper from '../common/ModalOptionsWrapper'
import Select from '../common/Select'
import { NormalizedWrapper, SelectWithOptionWrapper } from './styled'

const SelectWithOption = () => {
  const [selectedOption, setSelectedOption] = useState('')
  const [isModalOpen, setIsModalOpen] = useState(false)
  const modalRef = useRef<HTMLDivElement | null>(null)

  const handleOptionChange = (option: string) => {
    setSelectedOption(option)
    setIsModalOpen(false)
  }

  const toggleModal = () => {
    setIsModalOpen(!isModalOpen)
  }

  useClickOutside(modalRef, () => {
    setIsModalOpen(false)
  })

  return (
    <NormalizedWrapper>
      <SelectWithOptionWrapper>
        <Select selectedOption={selectedOption} onClick={toggleModal} />
        {isModalOpen && (
          <ModalOptionsWrapper
            innerRef={modalRef}
            onSelectOption={handleOptionChange}
            onClose={toggleModal}
          />
        )}
      </SelectWithOptionWrapper>
    </NormalizedWrapper>
  )
}

export default SelectWithOption
