import styled from 'styled-components'

export const OptionsWrapper = styled.div`
  width: 240px;
  height: 220px;
  border: 1px solid rgba(71, 194, 233, 0.18);
  border-radius: 4px;
`
