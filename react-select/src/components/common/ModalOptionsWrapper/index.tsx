import { optionsConfig } from 'src/constants/optionsConfig'

import ArrowRight from '../ArrowRight'
import OptionCard from '../OptionCard'
import { OptionsWrapper } from './styled'

interface ModalOptionsWrapperProps {
  onSelectOption: (option: string) => void
  onClose: () => void
  innerRef: React.Ref<HTMLDivElement>
}
const ModalOptionsWrapper: React.FC<ModalOptionsWrapperProps> = ({
  onSelectOption,
  onClose,
  innerRef,
}) => {
  return (
    <OptionsWrapper ref={innerRef}>
      {optionsConfig.map((option) => {
        return (
          <OptionCard
            key={option.id}
            option={option.name}
            onSelect={() => {
              onSelectOption(option.name)
              onClose()
            }}
          >
            <ArrowRight />
          </OptionCard>
        )
      })}
    </OptionsWrapper>
  )
}

export default ModalOptionsWrapper
