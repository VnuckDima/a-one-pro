import styled from 'styled-components'

export const SelectBorder = styled.div`
  width: 240px;
  height: 34px;
  border-radius: 4px;
  position: relative;
  padding: 2px;
  margin-bottom: 6px;
  background: linear-gradient(to right, #54f4df, #40d7ed, #31c4f5);
`

export const SelectWrapper = styled.div`
  border-radius: 4px;
  width: 100%;
  height: 100%;
  background: #1f2f34;
  position: relative;
  overflow: hidden;
  display: flex;
  align-items: center;
  justify-content: center;
  font-family: 'SofiaSans', sans-serif;
  font-weight: 600;
  font-size: 14px;
  letter-spacing: 0.05em;
  text-transform: uppercase;
  color: #bac1cc;
  cursor: pointer;
`
