import { SelectBorder, SelectWrapper } from './styled'

interface SelectProps {
  selectedOption: string
  onClick: () => void
}

const Select: React.FC<SelectProps> = ({ selectedOption, onClick }) => {
  return (
    <SelectBorder onClick={onClick}>
      <SelectWrapper>{selectedOption || 'account'}</SelectWrapper>
    </SelectBorder>
  )
}

export default Select
