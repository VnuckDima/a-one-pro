import { Option, OptionTitle } from './styled'

type OptionCardProps = {
  option: string
  children: React.ReactNode
  onSelect: () => void
}

const OptionCard: React.FC<OptionCardProps> = ({ option, onSelect, children }) => {
  return (
    <Option onClick={onSelect}>
      <OptionTitle>{option}</OptionTitle>
      <div>{children}</div>
    </Option>
  )
}

export default OptionCard
