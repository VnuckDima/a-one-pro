import styled from 'styled-components'

export const Option = styled.div`
  width: 100%;
  height: 44px;
  border-radius: 4px;
  display: flex;
  justify-content: space-around;
  align-items: center;
  border-bottom: 1px solid rgba(186, 193, 204, 0.1);
  background: #2b2c35;
  font-family: 'SofiaSans', sans-serif;
  font-weight: 600;
  font-size: 14px;
  letter-spacing: 0.05em;
  text-transform: uppercase;
  color: #bac1cc;
  cursor: pointer;

  &:last-child {
    border-bottom: none;
  }
`
export const OptionTitle = styled.div`
  width: 50%;
  height: 100%;
  display: flex;
  justify-content: left;
  align-items: center;
`
