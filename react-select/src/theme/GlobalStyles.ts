import { createGlobalStyle } from 'styled-components'

const GlobalStyles = createGlobalStyle`
  body {    
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  @font-face {
    font-family: 'SofiaSans';
    src: url('../assets/fonts/sofia-sans/SofiaSans.ttf') format('woff');
    font-weight: normal;
    font-style: normal;
  }

`

export default GlobalStyles
