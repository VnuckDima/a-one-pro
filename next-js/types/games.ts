export interface Game {
  identifier: string;
  title: string;
  provider: string;
  seo_title: string;
  categories: string[];
  category?: string;
}

export interface GameDetailProps {
  game: Game;
}
