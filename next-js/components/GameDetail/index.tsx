import { Game, GameDetailProps } from "@/types/games";
import { getGames } from "@/utils/api";
import { GetStaticPaths, GetStaticProps } from "next";
import Image from "next/image";
import { ParsedUrlQuery } from "querystring";

interface Params extends ParsedUrlQuery {
  provider: string;
  seo_title: string;
}

interface StaticProps {
  game: Game;
}

const GameDetail = ({ game }: GameDetailProps) => {
  return (
    <div className="max-w-2xl mx-auto my-8 p-6 border border-indigo-600 rounded-lg flex flex-col items-center justify-center bg-stone-300">
      <h1 className="text-4xl mb-4">
        Title: <strong>{game?.title}</strong>
      </h1>
      <p className="text-xl mb-2">
        Provider: <strong>{game?.provider}</strong>
      </p>
      <p className="text-xl mb-2">
        Categories:{" "}
        <strong>{game?.categories.join(", ") || "Not found"}</strong>
      </p>
      <p className="text-xl mb-2">
        SEO Title: <strong>{game?.seo_title}</strong>
      </p>
      {game && (
        <div className="mt-4">
          <Image
            width={300}
            height={300}
            src={`https://d2norla3tyc4cn.cloudfront.net/i/s3/${game.identifier}.webp`}
            alt={game.title}
            className="rounded-md"
            loading="lazy"
          />
        </div>
      )}
    </div>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  const games = await getGames();

  const paths = games.flatMap((game: Game) =>
    game.categories.map((category) => ({
      params: {
        provider: game.provider,
        category,
        seo_title: game.seo_title,
        title: game.title,
      },
    }))
  );

  return { paths, fallback: false };
};

export const getStaticProps: GetStaticProps<StaticProps, Params> = async ({
  params,
}) => {
  if (!params) {
    return {
      notFound: true,
    };
  }

  const { provider, seo_title, category } = params;
  const games = await getGames();

  const game = games.find(
    (g: Game) =>
      g.provider === provider &&
      g.category === category &&
      g.seo_title === seo_title
  );

  if (!game) {
    return {
      notFound: true,
    };
  }

  return {
    props: { game },
  };
};

export default GameDetail;
