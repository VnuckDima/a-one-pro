export async function getGames() {
    const response = await fetch(
      "https://nextjs-test-pi-hazel-56.vercel.app/data/games.json"
    );
    return response.json();
}