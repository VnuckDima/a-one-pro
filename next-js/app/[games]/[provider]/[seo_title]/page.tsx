import GameDetail from "@/components/GameDetail";
import { getGames } from "@/utils/api";
import { Game } from "@/types/games";

interface Props {
  params: {
    seo_title: string;
    provider: string;
    category: string;
  };
}

const GameInfo = async ({
  params: { seo_title, provider, category },
}: Props) => {
  const games = await getGames();

  const game = games.find(
    (g: Game) =>
      (g.provider === provider || g.category === category) &&
      g.seo_title === seo_title
  );

  return (
    <>
      <GameDetail game={game} />
    </>
  );
};

export default GameInfo;
