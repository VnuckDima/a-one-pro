
export const metadata = {
  title: "Next games info",
  description: "Next.js games info",
};

export default function Layout({ children }: { children: React.ReactNode }) {
  return (
    <main>
      {children}
    </main>
  );
}
