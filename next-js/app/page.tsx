import Link from "next/link";
import { getGames } from "@/utils/api";
import { Game } from "@/types/games";

const Home = async () => {
  
  const games = await getGames();

  return (
      <div className="flex flex-wrap justify-space-around ">
        {games.map((game: Game) => (
          <div
            key={game.identifier}
            className="border border-indigo-600 rounded-lg p-4 m-10 w-300 flex flex-col items-center h-350"
          >
            <Link
              href={`/games/${game.provider}/${game.seo_title}`}
              className="no-underline"
            >
              <h2 className="text-3xl text-center">{game.title}</h2>
            </Link>
            <p className="m-5 uppercase">Categories:</p>
            {game.categories.map((category) => (
              <Link
                className="text-2xl"
                key={`${game.provider}-${category}`}
                href={`/games/${category}/${game.seo_title}`}
              >
                <h3>{category}</h3>
              </Link>
            ))}
          </div>
        ))}
      </div>
  );
};



export default Home;
