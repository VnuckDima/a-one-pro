import React, { ReactNode } from "react";
import "./globals.css";

export const metadata = {
  title: "Next games",
  description: "Next.js games website",
};

export default function RootLayout({ children }: { children: ReactNode }) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  );
}
